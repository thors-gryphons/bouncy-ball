using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
     public void StartsGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex + 1 == 11 )
        {
            SceneManager.LoadScene("Game Won");
        }

    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("You have quitted the game now , just Imagine");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);

    }
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
