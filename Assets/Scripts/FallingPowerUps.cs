using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FallingPowerUps : MonoBehaviour
{
    [SerializeField] private GameObject[] powerUps;

    private BoxCollider2D boxCol2d;
    float x1, x2 , y1;
    float y = 6f;

    [SerializeField] public float height = 1.2f;
    private void Awake()
    {
        boxCol2d = GetComponent<BoxCollider2D>();
         x1 = transform.position.x - boxCol2d.bounds.size.x / 2f;
         x2 = transform.position.x + boxCol2d.bounds.size.x / 2f;
    }

    void Start()
    {
        StartCoroutine(SpawnPowerUps(4f));
    }

   IEnumerator SpawnPowerUps (float time)
    {
        yield return new WaitForSecondsRealtime(time);
        Vector3 temp = transform.position;
        temp.x = Random.Range(x1, x2);
        //temp.y += height;
        Instantiate(powerUps[Random.Range(0, powerUps.Length)], temp, Quaternion.identity);
        StartCoroutine (SpawnPowerUps(Random.Range(4f, 10f)));
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "SpikePowerUp")
        {
            SceneManager.LoadScene("Spike  Room Scene");
        }
    }

    //private void OnCollisionEnter2D(Collision2D col)
    //{
    //    if (col.gameObject.tag == "SpikePowerUp")
    //    {
    //        SceneManager.LoadScene("Spike  Room Scene");
    //    }
    //}
}
