using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerMoveUp : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    [Range (1,10)]
    public float smoothFactor;
    public Transform bg1;
    public Transform bg2;
    public Transform bg3;
    private float size;

    private void Start()
    {
        size = bg1.GetComponent<BoxCollider2D>().size.y;
    }


    private void FixedUpdate()
    {
        Follow();

        if (transform.position.y > bg2.position.y)
        {
            bg1.position = new Vector3(bg1.position.x, bg2.position.y + size, bg1.position.z);
            SwitchBG();
        }

        if (transform.position.y < bg1.position.y)
        {
            bg1.position = new Vector3(bg1.position.x, bg2.position.y - size, bg1.position.z);
            SwitchBG();
        }
    }
    private void SwitchBG()
    {
        Transform temp = bg1;
        bg1 = bg2;
        bg2 = bg3;
        bg3 = temp;
    }
    void Follow()
     {
         Vector3 targetPosition = new Vector3(0f, Mathf.Max(target.position.y, 0f), 0f) + offset;
         Vector3 smoothPosition = Vector3.Lerp(transform.position , targetPosition , smoothFactor*Time.fixedDeltaTime);
         transform.position = smoothPosition;

     }
}
