using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip playerJump, fireSound, portalSound;
    static AudioSource audioSrc;
    public static SoundManager Instance = null;
    public enum AudioClipName
    {
        PlayerJump = 0,
        FireSound = 1,
        PortalSound = 2
    }

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }

    public void PlaySound(AudioClipName audioClipName)
    {
        switch (audioClipName)
        {
            case AudioClipName.PlayerJump:
                audioSrc.PlayOneShot(playerJump);
                break;
            //case AudioClipName.FireSound:
            //    audioSrc.PlayOneShot(fireSound);
            //    break;
            case AudioClipName.PortalSound:
                audioSrc.PlayOneShot(portalSound);
                break;
        }
    }
}
