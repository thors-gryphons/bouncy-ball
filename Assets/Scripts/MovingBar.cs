using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBar : MonoBehaviour
{
    public Animator moveBar;
    public bool standOnIt = true;
    public GameObject barPrefab;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (barPrefab.tag == "Moving" && collision.gameObject.tag == "Player" && standOnIt)
        {
            moveBar.SetBool("standOnIt", false);
            Debug.Log("Moving");
        }
    }
}
