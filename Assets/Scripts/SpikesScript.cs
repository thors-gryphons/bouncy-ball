using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SpikesScript : MonoBehaviour
{
    public bool isNear = false;
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isNear = true;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
