using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    Rigidbody2D rb2d;
    [SerializeField]
    public float speed = 5f;
    public float jumpForce = 20f;
    private float horizontalMove;
    private int extraJump;
    public int extraJumpValue;
    bool playerControlsEnabled = true;
    public static PlayerScript Instance = null;
    
    public bool onGround;
    [SerializeField]
    private BoxCollider2D boxCol;
    [SerializeField]
    private CircleCollider2D cirCol;
    public Collider2D groundCheck;
    public LayerMask groundLayer;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.right * speed * Time.deltaTime;
        extraJump = extraJumpValue;

    }
    void Update()
    {
        onGround = groundCheck.IsTouchingLayers(groundLayer);
        if(onGround == true)
        {
            extraJump = extraJumpValue;
        }

        if (Input.GetKeyDown(KeyCode.Space)  && onGround)
        {
            rb2d.velocity = Vector2.right * Time.deltaTime;
            rb2d.AddForce(Vector2.up * jumpForce);
           onGround = false;
           //SoundManager.Instance.PlaySound(SoundManager.AudioClipName.PlayerJump);

        }
        if (extraJump > 0 && !onGround && Input.GetKeyDown(KeyCode.Space))
        {
            rb2d.AddForce(Vector2.up * 400);
            extraJump--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJump == 0 && onGround)
        {
            rb2d.AddForce(Vector2.up * jumpForce);

        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Platform" || other.gameObject.tag == "Trigger")
        {
            onGround = true;
        }
        if(other.gameObject.tag == "Wall")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }    
    }

    private void FixedUpdate()
    {
        if (playerControlsEnabled == true)
        { 
        horizontalMove = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(horizontalMove * speed, rb2d.velocity.y);
        }
    }
    public void PlayerLose()
    {
        playerControlsEnabled = false;
            rb2d.velocity = Vector2.zero;
            horizontalMove = 0;
    }

}

