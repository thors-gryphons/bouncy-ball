using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilt : MonoBehaviour
{
    public GameObject tiltBar;
    //private void OnTriggerEnter2D(Collider2D col)
    //{
    //    if(col.gameObject.tag == "Player")
    //    {
    //        Vector3 rotationVector = new Vector3(0,0,45);
    //        transform.rotation = Quaternion.Euler(rotationVector);
    //        Debug.Log("Rotate");

    //    }
    //}
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            float rotate = Random.Range(-90, 90);
            Vector3 rotationVector = new Vector3(0, 0, rotate);
            transform.rotation = Quaternion.Euler(rotationVector);
            Debug.Log("Rotate");
        }
    }
    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Vector3 rotationVector = new Vector3(0, 0, 0);
            transform.rotation = Quaternion.Euler(rotationVector);
            Debug.Log("At ease");
        }
    }


}
