using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomFire : MonoBehaviour
{
    public GameObject firePrefab;
    public GameObject barPrefab;
    public float fireSpeed = 5f;
    public Animator myAnimator;

    //private void OnTriggerEnter2D(Collider2D other)
    //{ 
    //    if(barPrefab.tag == "Trigger" && other.gameObject.tag == "Player")
    //    { 
    //        //Vector3 firePosition = barPrefab.transform.position;
    //        //Instantiate(firePrefab, firePosition, Quaternion.identity);
    //        myAnimator.SetBool("isFired", true);
    //        Debug.Log("Fire");
    //    }
    //}

    //private void OnTriggerExit2D(Collider2D other)
    //{
    //    myAnimator.SetBool("isFired", false);
    //    Debug.Log("Got Out");
    //}
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (barPrefab.tag == "Trigger" && other.gameObject.tag == "Player")
        {
            Vector3 firePosition = barPrefab.transform.position;
           GameObject fireBall =  Instantiate(firePrefab, firePosition, Quaternion.identity);
            Destroy(fireBall, 1f);
            myAnimator.SetBool("isFired", true);
            Debug.Log("Fire");
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (barPrefab.tag == "Trigger" && other.gameObject.tag == "Player")
        {
            myAnimator.SetBool("isFired", false);
            Debug.Log("Nope");
        }
    }

}
