using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltingBars : MonoBehaviour
{
    [SerializeField] float maxAngle = 45, smoothingFactor = 5f;
    [SerializeField] TriggerCheck.Operation operation = TriggerCheck.Operation.Freeze;
    Rigidbody2D rb2d;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        int direction = 0;
        if (operation == TriggerCheck.Operation.Left) direction = 1;
        else if (operation == TriggerCheck.Operation.Right) direction = -1;
        Quaternion desiredRotation = Quaternion.Euler(0f, 0f, maxAngle * direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, Time.deltaTime * smoothingFactor);
        if (operation == TriggerCheck.Operation.Center) rb2d.bodyType = RigidbodyType2D.Dynamic;
    }

    public void TriggerEntryDetected(Collider2D collision, TriggerCheck.Operation operation)
    {
        this.operation = operation;
    }
}
