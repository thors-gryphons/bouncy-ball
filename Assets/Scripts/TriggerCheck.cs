using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCheck : MonoBehaviour
{

    public enum Operation
    {
        Left = 0,
        Center = 1,
        Right = 2,
        Freeze = 3
    }

    TiltingBars bar;
    void Start()
    {
        bar = GetComponentInParent<TiltingBars>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        bar.TriggerEntryDetected(collision, (Operation)transform.GetSiblingIndex());
    }
}
