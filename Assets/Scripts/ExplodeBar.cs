using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExplodeBar : MonoBehaviour
{
    public bool isNear;
    public ParticleSystem parSys;

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            parSys.gameObject.SetActive(true);
            parSys.Play();
            parSys.transform.SetParent(null);
            Destroy(gameObject , 1f);
            Destroy(parSys, 1f);
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}
