using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingTile : MonoBehaviour
{
    public GameObject bar;
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (gameObject.tag =="Disappear")
        {
            Destroy(bar, 2f);
        }
    }
}
