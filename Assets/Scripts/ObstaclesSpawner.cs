using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpawner : MonoBehaviour
{
    public GameObject[] barsToInstantiate;
    Vector3 spawnPosition;
    public GameObject portalPrefab;
    public int numberOfBars = 5;
    public float height = 1.2f; //to adjust height between other obstacles
   [SerializeField]
    private float minX , maxX;
    void Start()
    {
        float y = -0.9f; //initial height of the first object
        for (int i = 0; i < numberOfBars; i++)
        {
            float x = Random.Range(minX, maxX);
            spawnPosition = new Vector3(x, y, 0f);
            y += height;
            if (i == numberOfBars -1)
            {
              Instantiate(portalPrefab, spawnPosition, Quaternion.identity);

            }
            else
            {
                RandomSpawn();
            }
        }

    }
    void RandomSpawn()
    {
        int n = Random.Range(0, barsToInstantiate.Length);
        Instantiate(barsToInstantiate[n], spawnPosition, Quaternion.identity);
    }
}
